MesseApp
========

L'applicazione del Liceo Messedaglia di Verona

Questa applicazione è distribuita gratuitamente sul Play Store di Google e il suo codice sorgente può essere utilizzato
secondo la seguente licenza creative commons:
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/deed.it"><img alt="Licenza Creative Commons" style="border-width:0" src="http://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Messeddaglia App</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://github.com/LookedPath/" property="cc:attributionName" rel="cc:attributionURL">LookedPath</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/deed.it">Creative Commons Attribuzione - Non commerciale - Condividi allo stesso modo 3.0 Unported License</a>.
